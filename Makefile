#

all: stm32f4_material.pdf

stm32f4_material.pdf: stm32f4_material.tex
	pdflatex stm32f4_material.tex

clear:
	rm -vf {*.bak, *.aux, *.bbl, *.blg, *.log}
