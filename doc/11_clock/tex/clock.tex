To get used to IDE, microcontroller and traversing datasheets a bit more elaborate examples are needed. In this chapter an attempt to use HAL and plunge into variety of peripherals will be made.

\section{Clock frequency}
Clock frequency is very important parameter to know and be able to configure as many operations rely on it start with delay routines and finishing with communication (e.g. using UART). 

MCU datasheet for STM32F411 has almost 50 pages long section about clock control. Clock tree presented in it is smaller than the block diagram presented in previous chapter, but not simpler. It contains many frequency multipliers and dividers and sometimes changing only main input frequency will be not enough. More than that, IDEs and various code libraries generate code or have multiple functions that manipulate the main clock frequency of the chip. All this makes it complicated to understand what frequency MCU is running at.

Backbone of clock distribution in STM32F4 microcontrollers is System Clock (SYSCLK) which can be set by three distinct sources: HSI (High Speed Internal), HSE (High Speed External) clocks and PLL (Phase-Locked Loop). SYSCLK supplies frequency to AHB and consequently to APB busses.

STM32 has a dedicated pin or pins depending on microcontroller complexity called \textbf{MCO} what stands for Main Clock Output and \textbf{MCOx} in case of a bigger microcontroller, where x is substituted by a number. They are used to outputting clock speed to microcontroller pin and measure frequency with an oscilloscope or frequency meter or to supply a clock source to an external circuitry.

From the clock tree presented in figure \ref{figure:clock-tree} several clock sources could be identified. These are \textbf{LSE} (low speed external), \textbf{HSE} (high speed external), \textbf{LSI} (low speed internal) and \textbf{HSI} (high speed internal) oscillators.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{clock-tree.png}
	\caption{STM32F411xE clock tree}
	\label{figure:clock-tree}
\end{figure}

\subsection{Example}
For most hobby applications internal oscillator will provide adequate accuracy, which may deviate from -8\% to 5.5\% according to electrical characteristics of the MCU. In the worst case it may lead to almost 2 hours error in timekeeping per day! When project requires more precise chronometry an external crystal is used. Typical crystal has accuracy of 100ppm\footnote{\textbf{P}arts \textbf{p}er \textbf{m}illion - a convention used to facilitate usage of small quantities.}, what results in 0.0001\% accuracy and 8.64 seconds per 24 hours.

A 8Mhz crystal is an external clock source on STM32F411 discovery kit. Internal oscillator is a 16Mhz RC resonator.

\subsubsection{The task}
The task of the current example is to configure MCU to work at different speeds by changing clock source and setting proper values of multipliers and dividers. Set up external oscillator and configure SYSCLK to maximum allowed frequency. For purposes of example HSE source will be enabled first and then used as an input to PLL to achieve 100Mhz\footnote{Maximum SYSCLK value for the STM32F411xE MCU.} system clock frequency.

\subsubsection{Solution}
Sequence of actions to change clock source of the MCU, which entails increase or decrease in CPU frequency is presented in section \textbf{Embedded Flash memory interface - Read interface - Relation between CPU clock frequency and Flash memory read time}. To ensure correct read of data from Flash memory latency should be imposed. It is dependent on CPU clock frequency and supply voltage of the device.

Section \textbf{Reset and clock control - Clocks - Clock-out capability} contains information about the pins and registers designated to outputting clock signal to a MCU pin. This signal may be used to source another IC or for debugging.

\texttt{MCO1PRE[2:0]} and \texttt{MCO1[1:0]} bits in \texttt{RCC\_CFGR} register will allow to output one of the following HSI, LSE, HSE and PLL clock signal to MCO1 pin (\texttt{PA8}).

\paragraph{HSI} After reset of a microcontroller it is sourced by HSI oscillator. It may be used as input to PLL or as SYSCLK directly. The HSI oscillator has an advantage of providing clock source at low cost. RC oscillator frequencies vary from device to device and therefore each MCU is factory calibrated for 1\% accuracy at 25\(^{\circ}\)C.

HSI oscillator signal is output to PA8 pin by default, configuration code that enables alternate function and configures MCO to output HSI clock signal is in file with a startup code. Connecting an oscilloscope to this pin may show a similar result to the one in figure~\ref{figure:hsi-signal}.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{HSI.png}
	\caption{HSI signal}
	\label{figure:hsi-signal}
\end{figure}

\paragraph{HSE} On a discovery board a 8Mhz resonator is connected to pins \texttt{OSC\_IN} and \texttt{OSC\_OUT}. MCU allows a much wider range of input signals. Crystal and ceramic resonators that generate signals from 4 to 26Mhz can operate as external clock source.

Enabling high speed external oscillator requires setting \texttt{HSEON} bit in \texttt{RCC\_CR} register and waiting hardware to set \texttt{HSERDY} flag. Switching from the HSI oscillator to the HSE oscillator will lower CPU frequency. Following list contains steps that developer need to undertake in that case.

\begin{enumerate}
	\item Modify the CPU clock source by writing to \texttt{RCC\_CFGR}.
	\item (If needed) Modify CPU clock prescaler.
	\item Check that new CPU clock source/prescaler are taken into account.
	\item Write new number of WS (Wait States) to \texttt{FLASH\_ACR} register.
	\item Check that the new number of WS is used.
\end{enumerate}

\begin{lstlisting}[language=C,caption=Enabling HSE as a SYSCLK]
/* Enabling HSE and waiting
* it to get ready */
uint32_t HSEStatus = 0;		// Status flag
RCC->CR |= RCC_CR_HSEON;		// enabling HSE
do
{
	HSEStatus = RCC->CR & RCC_CR_HSERDY;
} while(!HSEStatus)			// until HSE is ready
// Decreasing CPU frequency
RCC->CFGR |= RCC_CFGR_SWS_HSE;	// Select HSE as a new system clock source
while(1)	// Waiting for the clock source to be selected
{
	uint32_t tClockStatus = RCC->CFGR & RCC_CFGR_SWS_HSE;
	if(tClockStatus)
		break;
}
\end{lstlisting}

To be able to check if the source of signal changed \texttt{MCO1} needs to be reconfigured to output signal from another source. It is regulated by \texttt{RCC\_CFGR} register bits.

\begin{lstlisting}[language=C,caption=HSE to MCO1]
RCC->CFGR |= RCC_CFGR_MCO1_1;
\end{lstlisting}

After that modification output of the oscilloscope changed. Results of the changes are depicted in figure~\ref{figure:hse-signal}. It can be clearly seen that frequency dropped to 8Mhz.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{HSE.png}
	\caption{HSE signal}
	\label{figure:hse-signal}
\end{figure}

\paragraph{PLL} When maximum performance is required PLL\footnote{\textbf{P}hase \textbf{l}ocked \textbf{l}oop can track an input frequency, or it can generate a frequency that is a multiple of the input frequency.} is used. STM32F411 devices feature two PLLs: main clocked by HSE or HSI and may generate up to 100Mhz for system clock and 48Mhz for USB OTG, second is dedicated for generating an accurate clock to achieve high quality audio performance on the I2S interface.

Process of increasing CPU frequency is listed below:
\begin{enumerate}
	\item Write new numbers of Wait States to \texttt{FLASH\_ACR} register.
	\item Check if write was successful by reading.
	\item Modify CPU clock source by writing to \texttt{RCC\_CFGR} register.
	\item (If needed) Modify the CPU prescaler.
	\item Check that new CPU clock source/prescaler are taken into account.
\end{enumerate}

Since PLL configuration parameters cannot be changed once it is enabled, it is recommended to configured PLL before enabling it, select clock source and configure division and multiplication factors. MCU registers configure PLL output according to the formula:
\[f_{\text{VCO}} = f_{\text{PLL clock input}} * \frac{\text{PLLN}}{\text{PLLM}}\]
\[f_{\text{PLL clock output}} = \frac{f_{\text{VCO}}}{\text{PLLP}}\]
\[\therefore f_{\text{PLL clock output}} = \frac{f_{\text{PLL clock input}} * \frac{\text{PLLN}}{\text{PLLM}} }{\text{PLLP}}\]

In this equations PLLN, PLLM and PLLP should be edited with care, as not all values make acceptable values for hardware.

\begin{description}
	\item [PLLN] should be set correctly to ensure that \(f_{\text{VCO output}}\) is in range between 100Mhz and 432MHz. Therefore, \(50 \leqslant \text{PLLN} \leqslant 432\). Configuration of this factor is usually done after setting PLLM.
	\[f_{\text{VCO}} = f_{\text{VCO input}} * PLLN\]
	\item [PLLM] should be set correctly to ensure that \(f_{\text{VCO input}}\) is in range between 1Mhz and 2Mhz. Therefore, \(2 \leqslant \text{PLLM} \leqslant 63\). Configuration of this value is usually done in first place.
	\[f_{\text{VCO input}} = \frac{f_{\text{PLL input}}}{\text{PLLM}}\]
	\item [PLLP] may be equal to 2, 4, 6 or 8. Default value of this divider is 2, thus \(f_{\text{VCO}}\) should be at least two times the desired frequency.
\end{description}

To enable main PLL to output 100Mhz clock signal following values may be applied: \(\text{PLLN} = 100\), \(\text{PLLM} = 4\) and \(\text{PLLP} = 2\).

\begin{lstlisting}[language=c,caption=Configuring PLL variables and selecting PLL as SYSCLK]
// Configure and enable PLL
RCC->PLLCFGR |= (100 << 7);		// PLLN bits 14:6
RCC->PLLCFGR |= RCC_PLLCFGR_PLLM_2;		// Enabling 3rd bit, so PLLM = 4
RCC->CR |= RCC_CR_PLLON;
// Increase CPU frequency
FLSH->ACR |= 3;		// Program the new number of WS. 3WS at 100Mhz
while(!(FLASH->ACR & 3)){}	// Check that the new number of WS is taken into account
RCC->CFGR |= RCC_CFGR_SW_PLL;	// Modify the CPU clock source
while(!(RCC->CFGR & RCC_CFGR_SWS_PLL)){}	// Check that the new clock source is taken into account
\end{lstlisting}

To configure MCO1 to output PLL signal in addition to a miltiplexer selector a prescaler should be set, because otherwise oscilloscope will have troubles reading signal.

\begin{lstlisting}[language=c, caption=Configuring MCO1]
RCC->CFGR |= RCC_CFGR_MCO1;		// Will result in MCO1 = 0b11, as HSE was selected previously
RCC->CFGR |= RCC_CFGR_MCO1PRE_2;		// Set prescaler
\end{lstlisting}

Signal read from pin PA8 may resemble signal shown in figure~\ref{figure:pll-signal}.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{PLL.png}
	\caption{PLL signal}
	\label{figure:pll-signal}
\end{figure}

