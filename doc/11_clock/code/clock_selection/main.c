#include "stm32f4xx.h"                  // Device header

void InitAll();
void Delay(uint32_t pValue);
void EnableHSE(void);
void WaitButtonPress(void);
void EnablePLL100Mhz(void);

int main()
{
	InitAll();
	
	//RCC->CFGR |= RCC_CFGR_MCO1;		// output HSI to MCO pin PA8		// broke everything!! who knows why?
	WaitButtonPress();		// Wait button press
	EnableHSE();
	RCC->CFGR |= RCC_CFGR_MCO1_1;		// output HSE to MCO pin PA8
	WaitButtonPress();
	RCC->CFGR |= RCC_CFGR_MCO1;		// output PLL to PA8
	RCC->CFGR |= RCC_CFGR_MCO1PRE_2;
	EnablePLL100Mhz();
	
	GPIOD->BSRR = GPIO_BSRR_BS12;
	return 0;
}

void InitAll()
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;		// Enable port A
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;		// Enable port D
	//GPIOA->MODER |= 0x0;			// Input push button		// actually does nothing
	GPIOA->MODER |= GPIO_MODER_MODE8_1;
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR8;
	GPIOD->MODER |= GPIO_MODER_MODE12_0;		// Output PP		// Green LED
	GPIOD->MODER |= GPIO_MODER_MODE13_0;		// Orange LED
	
}

void EnableHSE()
{
	/* Enabling HSE and waiting
	* it to get ready	*/
	uint32_t HSEStatus = 0;
	RCC->CR |= RCC_CR_HSEON;		// enable HSE
	do
	{
		HSEStatus = RCC->CR & RCC_CR_HSERDY;
	} while(HSEStatus == 0);		// white until HSE is ready
}

void EnablePLL100Mhz()
{
	RCC->PLLCFGR |= RCC_PLLCFGR_PLLN_0 | RCC_PLLCFGR_PLLN_3 | RCC_PLLCFGR_PLLN_4;
	RCC->PLLCFGR |= RCC_PLLCFGR_PLLM_1;
	RCC->CR |= RCC_CR_PLLON;
}

void Delay(uint32_t pValue)
{
	for(; pValue > 0; pValue--);
}

void WaitButtonPress()
{
	uint32_t inputP0 = GPIOA->IDR & 1;
	while(1)
	{
		GPIOD->BSRR = GPIO_BSRR_BS13;
		Delay(1000000);
		GPIOD->BSRR = GPIO_BSRR_BR13;
		Delay(1000000);
		inputP0 = GPIOA->IDR & 1;
		if(inputP0 != 0) break;
	}
}
