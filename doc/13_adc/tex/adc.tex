\section{ADC}
In electronics system an analog to digital converter, abbreviated ADC, is a system that converts an analog input signal into a digital signal, sequence or one digital value. Analog signal is converted into discrete digital values by means of sampling and quantization. Different methods of ADC implementation exist. Successive approximation is one used in STM32F4 microcontroller. Way of operation of successive approximation ADC (SAR ADC) is identical to binary search algorithm. It may be slower than other implementations but due to very low power consumption it a common choice for low power devices. 

Tutorial by Walt Kester on successive approximation ADCs ''\textit{ADC Architectures II: Successive Approximation ADCs}`` will explain algorithm of operation of this type of analog to digital converter and acquaint with the history of ADC modules \autocite{successive-approximation-adc}. Also a first half of the ''ADC Tutorial: Analog to digital conversion`` article on Robot Platform website will give a basic understanding of how analog signal is converted into digital \autocite{robot-adc}.

STM32F411xE has a 12-bit successive approximation analog-to-digital converter. It also allows measuring from multiple external sources, two internal and V\(_{\text{BAT}}\) channel. Results of the measurements are stored in 16-bit register where data can be right- or left-aligned. 

Supply requirements for the ADC module are 2.4-3.6V at full speed and down to 1.8V at slower speed. This limits ADC V\(_{\text{REF+}}\) to 3.6V, but allowing safety margin 3.2V is the maximum value ADC accepts without a rist of damaging the microcontroller.

This section example uses STMicroelectronics HAL library. HAL is a modular library, where separate modules are called \textit{drivers}. STMicroelectronics has a lengthy, but very detailed document that describes the library. While working with the library refer to \textbf{UM1725} ''\textit{Description of STM32F4 HAL and LL drivers}`` as well as related to ADC sections in microcontroller datasheet and reference manual.

HAL library provides a generic and simple set of APIs to interract with the upper layer and ensures portability across the STM32 portfolio. It hides direct interactions with the MCU's registers and hides complex sequence of actions, e.g. increasing CPU frequency, behind a single function call.

\subsection{Example}
\subsubsection{Task}
Following example is supplied along with the STM32Cube firmware package for F4 family STM microcontrollers\footnote{This particular project is available in a firmware package of version 1.14.0 under following path \texttt{<root>/Projects/<your-board>/Examples/ADC/ADC\_RegularConversion\_DMA}.}.

It describes how to use the ADC and DMA to transfer data acquired during conversion to memory.

\subsubsection{Solution}
Project contains a brief description \textbf{readme.txt} explaining the operation of code of the example. Use of HAL library results in much more entangled code in comparison with use of CMSIS. However, complexity is equalized with extensive configuration checks and it saves many hours of looking for a problem in configuration caused by a misplaced bit.

Analog to digital converter in STM32F4 microcontroller is a very complex module with many features and registers related to it. First of all, familiarize yourself with electrical characteristics of ADC in this MCU. Documentation states that positive reference voltage cannot exceed supply voltage maximum value of which is 3.6V and maximum ADC clock frequency is limited to 36MHz. That section also shows calculation of conversion errors and typical connection diagram.

First of all proper software components for the project should be selected. Selection of \textbf{Device STM32Cube HAL - ADC} will pull many dependencies including drivers for \textbf{RCC, GPIO, DMA}. They are all necessary for the HAL proper operation and this project.

Having project open in IDE pay attention to project view. It has many more entries and files comparing to previous projects. Device entry contains all the enabled HAL files and source code directory now contains two more files \texttt{stm32f4xx\_it.c} and \texttt{stm32f4xx\_hal\_msp.c}. First one contains stubs for interrupt handlers and second one contains important ADC lol level resources initialization and deinitialization functions. They are mentioned in \textbf{UM1725} in section \textbf{HAL ADC generic driver - How to use this driver} and low level resource initialization is a mandatory step for using ADC with HAL.

Main function begins defines \texttt{ADC\_ChannelConfTypeDef} variable. HAL library uses configuration structures as a parameter of functions interacting with selected module. Then HAL library is being initialized by calling \texttt{HAL\_Init} function. It should be called before any other HAL function. \texttt{SystemClock\_Config} function sets the MCU to HSI as a PLL input and acquire 100Mhz at its output, so SYSCLK becomes 100MHz. APB2 is therefore 50Mhz and \(\text{ADC clock} = \frac{\text{APB2}}{2} = 25\text{Mhz}\). 

Now starts section where ADC is being configured. The ADC1 is configured to convert continuously channel8 (\texttt{ADC\_IN8}). In continuous mode, the ADC starts a new conversion as soon as it finishes one. ADC sampling time is set to 3 cycles, so the conversion to 12 bit result is 12 cycles. Thus, conversion time is \(\frac{12+3}{25*10^{6}} = 0.6*10^{-6}s\). 

\begin{lstlisting}[language=c, caption=ADC configuration]
AdcHandle.Instance = ADCx;		// ADC1 is being configured

AdcHandle.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2;
AdcHandle.Init.Resolution = ADC_RESOLUTION_12B;		// 12 bit resolution
AdcHandle.Init.ScanConvMode = DISABLE;
AdcHandle.Init.ContinuousConvMode = ENABLE;		// continious conversion
AdcHandle.Init.DiscontinuousConvMode = DISABLE;
AdcHandle.Init.NbrOfDiscConversion = 0;
// External conversion triggering is disabled
AdcHandle.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
AdcHandle.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T1_CC1;
AdcHandle.Init.DataAlign = ADC_DATAALIGN_RIGHT;		// Result is right aligned
AdcHandle.Init.NbrOfConversion = 1;		// Sequence of 1 conversions
AdcHandle.Init.DMAContinuousRequests = ENABLE;		// Direct memory access
AdcHandle.Init.EOCSelection = DISABLE;

if(HAL_ADC_Init(&AdcHandle) != HAL_OK)		// Write ADC configuration
	Error_Handler();		// Error configuring ADC
\end{lstlisting}

Converted values are stored into a ADC data register, but when DMA is used data is being stored in memory without CPU interference. That improves overall performance of the system.

After configuring the ADC operation input channel is configured.

\begin{lstlisting}[language=c, caption=ADC channel configuration]
ADC_ChannelConfTypeDef sConfig;

sConfig.Channel = ADCx_CHANNEL;		// ADC_CHANNEL_8
sConfig.Rank = 1;
sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
sConfig.Offset = 0;

if(HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
	Error_Handler();		// Channel configuration error
\end{lstlisting}

ADC driver can be used among three modes: polling, interruption and transfer by DMA. DMA mode execution flow is following.
\begin{itemize}
	\item Start ADC conversion using \texttt{HAL\_ADC\_Start\_DMA()} and specify the length of data to be transferred when conversion ends.
	\item At the end of data transfer \texttt{HAL\_ADC\_ConvCpltCallback()} function is executed and own code can be placed in it.
	\item In case of transfer error \texttt{HAL\_ADC\_ErrorCallback()} executed.
	\item Stop ADC conversion using \texttt{HAL\_ADC\_Stop\_DMA()}.
\end{itemize}

Finally, conversion is stared and when conversion ends DMA request is performed and 1 byte of data is being transferred to memory.
\begin{lstlisting}[language=c, caption=Enabling ADC operation]
if(HAL_ADC_Start_DMA(&AdcHandle, (uint32_t*)&uhADCxConvertedValue, 1) != HAL_OK)
	Error_Handler();
\end{lstlisting}

