\section[USART]{Universal syncronous asynchronous receiver/transmitter}
A universal asynchronous receiver/transmitter or simply UART is a hardware device for asynchronous serial communication. UART are commonly included in microcontrollers. STM32F4 series MCU may have up to 10 UARTs interfaces in a single device \autocite{stm32f4-line}. On STM32F4 microcontroller USARTs allow full-duplex data exchange with wide range of baud rates using a fractional baud rate generator. It also supports half-duplex single wire communication.

A half-duplex system is a point-to-point system of two devices, where each device can communicate with other but not simultaneously. A full-duplex system is a point-to-point system of two devices, where both devices can communicate with each other simultaneously \autocite{networks-tanenbaum}.

\subsection{Example}
\subsubsection{Task}
This example describes a UART transmission (transmit/recieve) in interrupt mode between two boards.

For this tutorial another discovery board will be required. If you have no access to second board it can be replaced with another microcontroller having an UART module or UART to USB board like the one in picture \ref{figure:uart-to-usb}.

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{uart-to-usb.png}
	\caption{CP2102 UART to USB module}
	\label{figure:uart-to-usb}
\end{figure}

In this tutorial USART2 will be used. Pin definition table for STM32F411xE microcontroller\footnote{Located in microcontroller datasheet} says that PA2 is USART2\_TX, second USART module transmission pin, and PA3 is USART2\_RX, receiver pin of USART module. Therefore, RX pin of the first board should be connected to TX pin of the second and vice versa.

\subsubsection{Solution}
At the beginning of the main program the \texttt{HAL\_Init()} function is called. Then the \texttt{SystemClock\_Config()} function is used to configure the system clock (SYSCLK) to 100 MHz.

Then 1st board is waiting for user button key to be pressed - orange LED is blinking on transmitter board. This code will be compiled only for Transmitter board, for Receiver board \texttt{TRANSMITTER\_BOARD} definition should be commented out.
\begin{lstlisting}[language=C, caption=Transmitter board waits for user to press a button]
#ifdef TRANSMITTER_BOARD
  /* Configure USER Button */
  BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);
  /* Wait for USER Button press before starting the Communication */
  while (BSP_PB_GetState(BUTTON_KEY) == RESET)
  {
    /* Toggle LED3 waiting for user to press button */
    BSP_LED_Toggle(LED3);
    HAL_Delay(40);
  }
  /* Wait for USER Button to be release before starting the Communication */
  while (BSP_PB_GetState(BUTTON_KEY) == SET) {}
  /* Turn LED3 off */
  BSP_LED_Off(LED3);
#endif /* TRANSMITTER_BOARD */
\end{lstlisting}

In this section of code previously not mentioned delay function is used. \texttt{HAL\_Delay} implements a delay expressed in milliseconds.

After that USART module is configured to transmit in asynchronous mode (UART mode) and due to other electronic devices are connected to Tx and Rx pins of the UART1 on discovery board UART2 should be used.
\begin{lstlisting}[language=C, caption=Configuration of UART2]
UartHandle.Instance          = USARTx;		// USARTx = USART2
UartHandle.Init.BaudRate     = 9600;
UartHandle.Init.WordLength   = UART_WORDLENGTH_8B;		// word length = 8bits
UartHandle.Init.StopBits     = UART_STOPBITS_1;		// one stop bit
UartHandle.Init.Parity       = UART_PARITY_NONE;		// parity = none
UartHandle.Init.HwFlowCtl    = UART_HWCONTROL_NONE;		// no hardware control
UartHandle.Init.Mode         = UART_MODE_TX_RX;
UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;

if(HAL_UART_Init(&UartHandle) != HAL_OK)		// Initialization of UART
  Error_Handler();
\end{lstlisting}

Without HAL library procedure of transmission of data via USART is following:
\begin{enumerate}
	\item Enable the USART by setting UE bit in USART\_CR1 register.
	\item Set word length by programming M bit in USART\_CR1 register.
	\item Program the number of stop bits.
	\item If DMA is used enable it and configure as explained in \textit{multibuffer communication} section in reference manual of microcontroller.
	\item Select the desire baud rate by programming USART\_BRR register. (Fractional generator is used, so value that would be programmed in BRR register should be calculated first.)
	\item Write the data to send in USART data register and repeat this for each data to be transmitted.
	\item After writing the last data byte wait until indication that transmission is complete (TC=1).
\end{enumerate}

When using HAL library sequence is reduced to four, one of which is optional, more simple and understandable steps. Simplification of the code is not the only reason this library is very attractive. It makes code portable and the same code may be used without any changes on other STM32 microcontrollers.
\begin{enumerate}
	\item Declare a \texttt{UART\_HandleTypeDef} structure.
	\item* Initialize the low level resource by calling \texttt{HAL\_UART\_MspInit()}, if USART module is in reset state \texttt{HAL\_UART\_Init()} performs it automatically.
	\item Program the baud rate, word length, stop bit count, party, hardware flow control and mode of operation in the init structure.
	\item For the UART intialize \texttt{HAL\_UART\_Init()}
\end{enumerate}

After that transmitting board programs  DMA for reception before starting the transmission, in order to ensure DMA Rx is ready when board 2 will start transmitting. \texttt{HAL\_UART\_Recieve\_DMA()} is non-blocking function call, thus execution will proceed to the next commands being able to receive data from Rx pin. Next command is transmitting data using DMA access.
\begin{lstlisting}[language=C, caption=Board 1 transmitts and waits for respond]
#ifdef TRANSMITTER_BOARD
  /* The board sends the message and expects to receive it back */
  /* DMA is programmed for reception before starting the transmission, in order to
     be sure DMA Rx is ready when board 2 will start transmitting */
  /*-2- Program the Reception process */
  if(HAL_UART_Receive_DMA(&UartHandle, (uint8_t *)aRxBuffer, RXBUFFERSIZE) != HAL_OK)
    Error_Handler();

  /*-3- Start the transmission process */
  /* While the UART in reception process, user can transmit data through
     "aTxBuffer" buffer */
  if(HAL_UART_Transmit_DMA(&UartHandle, (uint8_t*)aTxBuffer, TXBUFFERSIZE)!= HAL_OK)
    Error_Handler();
  /*-4- Wait for the end of the transfer */
  while (UartReady != SET) {}
  /* Reset transmission flag */
  UartReady = RESET;
#else
  // RECIEVER_BOARD
#endif	// Same for both receiver and transmitter
 /*-5- Wait for the incomming transmission */
  while (UartReady != SET) {}
  /* Reset transmission flag */
  UartReady = RESET;
  /*-6- Compare the sent and received buffers */
  if(Buffercmp((uint8_t*)aTxBuffer,(uint8_t*)aRxBuffer,RXBUFFERSIZE))
    Error_Handler();
  while (1) {} /* Infinite loop */
\end{lstlisting}

Transmitter board after sending the data will wait in loop for another board to respond with the same message. It will be waiting until \texttt{HAL\_UART\_RxCpltCallback()} is executed due to received data at Rx pin.

Buffer are being compared byte to byte.
\begin{lstlisting}[language=C, caption=Buffer comparison]
static uint16_t Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength)
{ 
  while (BufferLength--)
  {
    if((*pBuffer1) != *pBuffer2)	// byte to byte comparison
    {
      return BufferLength;
    }
    pBuffer1++;
    pBuffer2++;
  }
  return 0;
}
\end{lstlisting}

Receiver at the same time initiates receiving with DMA transfer and wait for incoming message from transmitter blinking with orange LED at the meantime.
\begin{lstlisting}[language=C, caption=Receiver operation]
#else
  /* The board receives the message and sends it back */
  /*-2- Put UART peripheral in reception process */
  if(HAL_UART_Receive_DMA(&UartHandle, (uint8_t *)aRxBuffer, RXBUFFERSIZE) != HAL_OK)
    Error_Handler();
  /*-3- Wait for the end of the transfer */
  /* While waiting for message to come from the other board, LED2 is
     blinking according to the following pattern: a double flash every half-second */
  while (UartReady != SET)
  {
      BSP_LED_On(LED3);
      HAL_Delay(100);
      BSP_LED_Off(LED3);
      HAL_Delay(100);
      BSP_LED_On(LED3);
      HAL_Delay(100);
      BSP_LED_Off(LED3);
      HAL_Delay(500);
  }
  /* Reset transmission flag */
  UartReady = RESET;
  BSP_LED_Off(LED3);
  /* Reset transmission flag */
  UartReady = RESET;
  /*-4- Start the transmission process */
  /* While the UART in reception process, user can transmit data through
     "aTxBuffer" buffer */
  if(HAL_UART_Transmit_DMA(&UartHandle, (uint8_t*)aTxBuffer, TXBUFFERSIZE)!= HAL_OK)
    Error_Handler();
#endif	// Same for both receiver and transmitter 
// waiting for incomming message
// comparison of sent and received messages
\end{lstlisting}

Warning: As both boards do not behave same way, "TRANSMITTER\_BOARD" switch compilation exists to determine either software is for 1st transmitter board or 2nd receiver (then transmitter) board. In other words, 1st board has to be flashed with software compiled with switch enable, 2nd board has to be flashed with software compiled with switch disable.

STM32 discovery board's LEDs can be used to monitor the transfer status:
\begin{itemize}
	\item LED3 (Orange) is blinking on master side waiting for user to press button
	\item LED6 (blue) is ON when the transmission process is complete.
	\item LED4 (green) is ON when the reception process is complete.
	\item LED3 (Orange) is ON when there's UART error callback, otherwise the Led is blinking waiting for button press to start transmission.
	\item LED5 (red) is ON when there is an error in transmission/reception process.
 \end{itemize}

The UART is configured as follows:
\begin{itemize}
	\item BaudRate = 9600 baud  
	\item Word Length = 8 Bits (7 data bit + 1 parity bit)
	\item One Stop Bit
	\item None parity
	\item Hardware flow control disabled (RTS and CTS signals)
	\item Reception and transmission are enabled in the time
\end{itemize}

When firmware is executed on both boards lit blue and green LEDs notify about successful sending and receiving the message. Any other of two user LEDs left would notify about some kind of problem while execution or communication.

If you connect board to PC using previously shown UART to USB module to discover what is being transmitted you will get result shown in figure \ref{figure:terminal}. By sending back same message you will emulate operation of 2nd (receiver) board.
\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{terminal.png}
	\caption{RealTerm 2.0 receving data from STM32F4 microcontroller}
	\label{figure:terminal}
\end{figure}
