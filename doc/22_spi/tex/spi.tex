\section[SPI]{Serial peripheral interface}
The Serial Peripheral Interface (SPI) is a synchronous communication interface, that allows a duplex, synchronous, serial communication with external devices. Usually the SPI in connected to external devices through four pins:
\begin{description}
	\item[MISO] Master In / Slave Out data. Transmits data in slave mode and receives in master mode.
	\item[MOSI] Master Out / Slave In data. Transmits data in master mode and receives in slave mode.
	\item[SCK] Serial Clock output. SPI master generates clock signal and slaves accept it.
	\item[nSS] Slave Select. An optional pin to select a slave device and communicate with slaves individually.
\end{description}

The SPI can operate with a single master and one or more slave devices. Communication starts when master configures clock with a frequency supported by the slave device and selects the slave device with logic 0 on SS (slave select) line. Slave devices that are not selected do not interfere with SPI bus activities. During each clock cycle a full duplex data transmission occurs. For more information in the interface, how modules are structured and communication takes place refer to Motorola \textit{SPI Block Guide} \autocite{motorolla-spi}.

\subsection{Example}
This example is also a part of STM32Cube firmware package and uses HAL library. Its structure is similar to USART example. Code has parts are are available for transmitting board and not available for receiving one. The converse is also true. Therefore, in this tutorial main accent will be made on review of SPI configuration and HAL functions.

\subsubsection{Task}
The task of the code is to make one board transmit and another one to accept message and finish by comparing integrity of the transmitted message on both devices.

\subsubsection{Solution}
Main function starts with initialization of HAL environment and increasing CPU clock speed to 100MHz. Then SPI initialization structure is being filled with data. It specifies SPI module in microcontroller, clock prescaler for SCK frequency, edge of bit capture, serial clock steady state, cyclic redundancy check, transmission data size, slave selection by hardware or software. This configuration is devined for both transmitter and receiver boards.
\begin{lstlisting}[language=C, caption=Programming of SPI initialization structure]
/* Set the SPI parameters */
SpiHandle.Instance               = SPIx;	// SPIx = SPI2
SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;	// SCK prescaler
SpiHandle.Init.Direction         = SPI_DIRECTION_2LINES;	// full-duplex
SpiHandle.Init.CLKPhase          = SPI_PHASE_1EDGE;		// active edge for bit capture
SpiHandle.Init.CLKPolarity       = SPI_POLARITY_HIGH;	// steady state
SpiHandle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;		// CRC
SpiHandle.Init.CRCPolynomial     = 7;
SpiHandle.Init.DataSize          = SPI_DATASIZE_8BIT;		// data size
SpiHandle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
SpiHandle.Init.NSS               = SPI_NSS_SOFT;	// software selection of slave
SpiHandle.Init.TIMode            = SPI_TIMODE_DISABLE;	// TI mode

#ifdef MASTER_BOARD
  SpiHandle.Init.Mode = SPI_MODE_MASTER;	// transmitter
#else
  SpiHandle.Init.Mode = SPI_MODE_SLAVE;		// receiver
#endif /* MASTER_BOARD */

if(HAL_SPI_Init(&SpiHandle) != HAL_OK)
  Error_Handler(); //initialization error
\end{lstlisting}

Second SPI module defines microcontroller pins that will be used for data transmission and clock output/input. Interconnection of two boards is presented in figure \ref{figure:spi-connection}.
\begin{itemize}
	\item SCK - PB13
	\item MISO - PB14
	\item MOSI - PB15
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{connection.png}
	\caption{SPI connection}
	\label{figure:spi-connection}
\end{figure}

SPI HAL firmware driver expects following sequence of actions to operate:
\begin{enumerate}
	\item Declare \texttt{SPI\_HandleTypeDef} initialization structure.
	\item * Initialize the SPI low level resources by calling \texttt{HAL\_SPI\_MspInit()}. If it was not done previously \texttt{HAL\_SPI\_Init()} will perform initialization.
	\item Program the required mode, data size, clock polarity and phase and other preferences in the initialization structure. 
	\item Initialize the SPI by calling \texttt{HAL\_SPI\_Init()}.
\end{enumerate}

The SPI peripheral configuration is ensured by the \texttt{HAL\_SPI\_Init()} function. This later is calling the \texttt{HAL\_SPI\_MspInit()} function which core is implementing the configuration of the needed SPI resources according to the used hardware (CLOCK, GPIO and NVIC). You may update this function to change SPI configuration.

The SPI communication is then initiated. The \texttt{HAL\_SPI\_TransmitReceive\_IT()} function allows the reception and the transmission of a predefined data buffer at the same time (Full Duplex Mode). It is executed in non-blocking mode with interrupt executed at the end of operation.
\begin{lstlisting}[language=C, caption=Starting transmission]
if(HAL_SPI_TransmitReceive_IT(&SpiHandle, (uint8_t*)aTxBuffer, (uint8_t *)aRxBuffer, BUFFERSIZE) != HAL_OK)
  Error_Handler();		// transfer error
\end{lstlisting}

Then microcontroller waits for the transmission to finish. In real project will be probably performing other tasks instead of just waiting. After that the messages are being compared.
\begin{lstlisting}[language=C, caption=Waiting for transmission to finish]
while (HAL_SPI_GetState(&SpiHandle) != HAL_SPI_STATE_READY) {}
if(Buffercmp((uint8_t*)aTxBuffer, (uint8_t*)aRxBuffer, BUFFERSIZE))
  Error_Handler(); 	// messages are not the same -> error while transmission
\end{lstlisting}

The user can choose between Master and Slave through \texttt{\#define MASTER\_BOARD} in the "main.c" file.  If the Master board is used, the \texttt{\#define MASTER\_BOARD} must be uncommented. If the Slave board is used the \texttt{\#define MASTER\_BOARD} must be commented.

For this example the aTxBuffer is predefined and the aRxBuffer size is same as aTxBuffer. In a first step after the user press the User Key, SPI Master starts the communication by sending aTxBuffer and receiving aRxBuffer through HAL\_SPI\_TransmitReceive\_IT(), at the same time SPI Slave transmits aTxBuffer and receives aRxBuffer through HAL\_SPI\_TransmitReceive\_IT(). The end of this step is monitored through the HAL\_SPI\_GetState() function result.  Finally, aRxBuffer and aTxBuffer are compared through Buffercmp() in order to check transmitted message integrity.
\begin{lstlisting}[language=C, caption=Buffer comparison]
static uint16_t Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength)
{ 
  while (BufferLength--)
  {
    if((*pBuffer1) != *pBuffer2)	// byte to byte comparison
    {
      return BufferLength;
    }
    pBuffer1++;
    pBuffer2++;
  }
  return 0;
}
\end{lstlisting}

STM32 Discovery board's LEDs can be used to monitor the transfer status:
\begin{itemize}
	\item LED3 (orange) toggles on master board waiting user button to be pressed.
	\item LED4 (green) turns ON when the transmission process is complete.
	\item LED6 (blue) turns ON when the reception process is complete.
	\item LED5 (red) turns ON when there is an error in transmission/reception process.
\end{itemize}

In addition to interrupt handler routine HAL callbacks are used to execute user code in case of successful transmission and error.
\begin{lstlisting}[language=C, caption=HAL SPI callbacks]
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
  /* Turn LED4 on: Transfer in transmission process is correct */
  BSP_LED_On(LED4);
  /* Turn LED6 on: Transfer in reception process is correct */
  BSP_LED_On(LED6);
}
 void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi)
{
  /* Turn LED5 on: Transfer error in reception/transmission process */
  BSP_LED_On(LED5);
}
\end{lstlisting}
