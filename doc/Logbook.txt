Date: Wed 02 Nov 2016
Aim: Project on CU Ethics system

What is research question and if it can be applied to my work?

What is the type of study that will be used? To achieve main goal of the project qualitative approach to study should by applied. Results should answer questions of type "how" and "why". 

What will be the research design? An experiment design will be attempted to gradually envestigate different features of the device.




Date: Sun 13 Nov 2016
Aim: Cortex-M4 introduction


Document used: ARM Cortex-M4 Processor (ARM 2015 technical reference manual)

The Cortex-M4 processor is a low-power processor that features low gate count, low interrupt latency, low cost debug. It may include floating point arithmetic functionality (optional). 

Its features include: processor core, Nested Vectored Interrupt Controller (NVIC), multiple bus interfaces, (optional) Memory Protection Unit (MPU), (optional) Floating Point Unit (FPU).

The processor supports two modes of operation: Thread and Handler modes. Code can execute as privileged or unprivileged. Unprivileged exectution limits or exludes access to some resources. Privileged execution has access to all resources. Handler mode is always privileged. Thread mode can be privileged or unprivileged.




Date: Tue 15 Nov 2016
Aim: Cortex-M4 bit-band


Bit-banding is an optional feature of the Cortex-M4 processor. Bit-banding maps a complete word of memory onto a single bit in the bit-band region. For example, writing to one of the alias words sets or clears the corresponding bit in the bit-band region. This enables every individual bit in the bit-banding region to be directly accessible from a word-aligned address using a single processor instruction. It also enables individual bits to be toggled without performing a read-modify-write sequence of instructions.

The processor memory map includes two bit-band regions (lowest 1MiB of the SRAM and Peripheral memory regions).

Directly accessing an alias region: writing to a word in the alias region has the same effect as a read-modify-write operation on the targeted bit in the bit-band region. Bit [0] of the value written to a word in the alias region determines the value written to the targeted bit in the bit-band region. Writing a value with bit [0] set writes a 1 to the bit-band bit, and writing a value with bit [0] cleared writes a 0 to the bit-band bit. Bits [31:1] of the alias word have no effect on the bit-band bit.

Directly accessing a bit-band region: This region can be directly accessed with normal reads and writes.




Date: Wed 16 Nov 2016
Aim: Cortex-M4 registers


The processor has the following 32-bit registers:
	13 general purpose registers r0-r12
	Stack pointer (SP), alias of banked SP_process and SP_main registers
	Link register (LR), r14
	Program counter (PC), r15
	Special purpose Program Status Register (xPSR)

The general purpose registers r0-r12 have no special architecturally defined uses. Most instructions that can specify a general purpose register can specify r0-r12. However, they can be splitted in two logical groups.
Low registers - Registers r0-r7 are accessible by all instructions that specify a general purpose register.
High registers - Registers r8-r12 are accessible by all 32-bit instructions that specify a general purpose register and not accessible by all 16-bit instructions.

Registers r13, r14 and r15 have the following special functions.
Stack pointer - Register r13 is used as the Stack Pointer (SP). Because the SP ignores writes to bits [1:0], it is autoaligned to a word, four-byte boundary.
Link register - Register r14 is the subroutine Link Register (LR). The LR receives the return address from PC when a Branch and Link or Branch and Link with Exchange (BL, BLX) instruction is executed. The LR is also used for exception return. At all other times, it can be treated as a general purpose register.
Program counter - Register r15 is the Program Counter (PC). Bit [0] is always 0, so instructions are always aligned to word or halfword boundaries.


Date: Thu 17 Nov 2016
Aim: Cortex-M4 exceptions


The processor and the Nested Vectored Interrupt Controller (NVIC) prioritize and handle all exceptions. Exception handling:
	All exceptions are handled in Handler mode.
	Processor state is automatically stored to the stack on an exception and automatically restored from the stack at the end of the Interrupt Service Routine (ISR).
	The vector is fetched in parallel to state saving, enabling efficient interrupt entry.

Late-arrival and tail-chaining mechanisms are used to reduce interrupt latency.
	There is a maximum of a 12 cycle latency from asserting the interrupt to execution of the first instruction of the ISR when the memory being accessed has no wait states being applied.
	Returns from interrupts similarly take twelve cycles where the instruction being returned to is fetched in parallel to the stack pop.
	Tail chaining requires 6 cycles when using zero wait state memory. No stack pushes or pops are performed and only the instruction for the next ISR is fetched.

The processor exception model has the following implementation-defined behavior in addition to the architecturally defined bahavior.
	Exceptions on stacking from HardFault NMI lockup at NMI priority
	Exception on unstacking from NMI to HardFault lockup at HardFault priority.

To minimize interrupt latency, the processor abandons any divide instructions to take any pending interrupt. On return from the interrupt handler, the processor restarts the divide instruction from the beginning.




Date: Sat 19 Nov 2016
Aim: Cortex-M4 configuration and additional units overview


There are plenty of system control registers, more than 20. This registers are used to configure core operation flow and modules.

Memory Protection Unit (MPU). The MPU is an optional component for memory protection. The MPU provides full support for:
	protection regions
	overlapping protecting regions, with ascending region priority
	access permissions

MPU mismatches and permission violations invoke the programmable-priority MemManage fault handler.


Nested Vectored Interrupt Controller (NVIC). The NVIC provides configurable interrupt handling abilities the the processor. It supports up to 240 interrupts each with up to 256 levels of priority. Developer can change the priority of an interrupt dynamically. The NVIC maintains knowledge of the stacked or nested interrupts to enable tail-chaining of interrupts.

The NVIC can only be fully accessed from privileged mode.

The processor supports both level and pulse interrupts. A level interrupt is held asserted until it is cleared by the ISR accessing the device. A pulse interrupts is a variant of an edge model.


Floating point unit (FPU). The Cortex-M4 FPU is an implementation of the single precision variant of the ARMv7-M Floating Point Extension. It provides floating-point computation functionality that is compliant with the IEEE 754 standard. The FPU fully supports single-precision add, subtract, multiply and square root operations. It also provide conversion operations. The FPU provides an extension register file containing 32 single-precision registers.




Date: Mon 21 Nov 2016
Aim: STM32F4 board introduction

UM1472 document was used to create initial document on Discovery board itself.
[external file: 1_stm32f4-disc-description.txt]



Date: Tue 22 Nov 2016
Aim: STM32F4 board introduction

[external file: 1_stm32f4-disc-description.txt]



Date: Wed 24 Nov 2016
Aim: Toolchain selection

[external file: 2_toolchain.txt]



Date: Sat 26 Nov 2016
Aim: IDEs configuration

ST provides UM1467 document that describes how to configure four IDEs which include EWARM, MDK-ARM, TrueSTUDIO and TASKING VX-toolset for ARM by Altium.

Cube firmware package however covered very briefly explaining only package tree structure and folder contents without going into details what libraries are inside and what they are responsible for. Document is also outdated as currently StdPeriph folder is not included in the distributable package.

[link: UM1467.pdf]
