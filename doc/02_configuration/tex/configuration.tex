\section{First program}
In first example we will create a simple program that turns LEDs on and off. Main tutorials objectives are to ensure toolchain is set up and configured correctly, firmware could be uploaded to the chip's memory, and to show how to use documentation and how to find the information needed to achieve your goal.

\subsection{The task}
Discovery board has four LEDs so called \textit{user LEDs}. They are not used by ST-Link or by circuitry to notify about over current, so developers can use them without a fear to break any existing functionality. On board these are LD3 through LD6.

The task is to simply turn these LEDs on and after a delay turn them off. Along with that understand structure of a simple project and get acquaint with CMSIS-core files for the MCU.

\subsection{Solution}
Start with opening documentation for the Discovery board. For 32F411EDISCOVERY it is \textbf{UM1842}, obtainable from ST website. Skimming through the table of contents you will see two entries important for this task. They are \textbf{Hardware layout - LEDs} and \textbf{Electrical schematic}.

First section contains information about pins LEDs connected to, whereas, schematics (figure~\ref{figure:discovery-leds}) shows how diodes are connected\footnote{In documentation of 1st revision Figure 14. Peripherals}.
\begin{itemize}
	\item The orange LED is a user LED connected to the I/O PD13\footnote{PD13 stands for Port D pin 13 (counting starts from 0). Generalized formula is PXY, where X - port letter and Y - pin number.}.
	\item The green LED is a user LED connected to I/O PD12.
	\item The red LED is a user LED connected to I/O PD14.
	\item The blue LED is a user LED connected to I/O PD15.
\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{leds.png}
	\caption{LEDs connections}
	\label{figure:discovery-leds}
\end{figure}

Knowing which ports to work with another let's launch IDE and create new project. If you are not familiar with uVision IDE \textbf{Getting started with MDK version 5} is there to help. Assuming you have Keil MDK and software pack for your device installed use menu \textbf{Project - New uVision Project}. Specify project directory, then select target device\footnote{Only devices of the installed Software Packs are shown. If device is missing in the list use Pack Installer to add the related pack.}.

After device selection, the \textbf{Runtime environment} window is shown. Select \textbf{Device - Startup} and \textbf{CMSIS - Core}. First is needed for the MCU initial configuration and proper startup. It usually referred as ''Bootloader``. Second is the header file with macro definitions that simplify work with the registers of the MCU. Upon creation a project tree will look like the one presented in figure~\ref{figure:project-tree}. Selected packs are shown in the tree with green rhombuses. Source group, \texttt{Source Group 1} in picture, is a container for source files. It allows logical separation of files. At this point everything is ready for writing the code and you may start with a following template.

\begin{figure}
	\centering
	\includegraphics{project-tree.png}
	\caption{Project tree}
	\label{figure:project-tree}
\end{figure}

\begin{lstlisting}[language=C, caption=main.c - Entry point of the firmware]
#include "stm32f4xx.h"		// header that based on macro definition includes a more specific header

int main()
{
	return 0;
}
\end{lstlisting}

From this point onward we are interested mainly in documentation on the microcontroller that is installed on the discovery board. Dedicated STMicroelectronics web page contains many documents on different applications, explanation of internal processes and modules implementation. We will focus on four documents for now: \\

\noindent\begin{tabular}{r l}
	\textbf{Document} & \textbf{Description} \\
	RM0383 & STM32F411xC/E Reference manual \\
	PM0214 & STM32F3, STM32F4 and STM32L4 series programming manual \\
	ES0287 & STM32F411xC and STM32F411xE device limitations \\
	DocID026289 & STM32F411 MCU Datasheet \\
\end{tabular}\\[0.3cm]

First document is the most important because it describes how to configure MCU and use its capabilities. Errata sheet, the third document, complements the reference manual by providing additional information about microcontroller features, but main purpose of this document is to point on errors and bugs in documentation and MCU itself. The programming manual is more generalized document describing command set of Cortex-M4 processor and it is more beneficial to operating system developers.

Code shown above does nothing, but compiles. To compile press \textbf{build} button or \textbf{F7} on a keyboard to build the project. Willing to turn on LED we should refer the reference manual (RM0383) and MCU data-sheet first. The reference manual is a lengthy document in a form of a catalog, what means that every section is logically separate from others and does not require reading preceding sections. Whenever need arises refer to specified section of the manual, all the references to other section of the manual are clearly stated where necessary.

From datasheet we acquire microcontroller block diagram, where all the peripherals are shown. This block diagram is presented in the figure~\ref{figure:block-diagram-gpiod} and it is seen from it that GPIO port D is connedted to AHB1 bus.
\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{block-diagram-gpiod.jpg}
	\caption{STM32F411 block diagram}
	\label{figure:block-diagram-gpiod}
\end{figure}

Section \textbf{Memory and bus architecture - AHB/APB bridges} mentions about very important setting typical to STM32 microcontrolles done to minimize power consumption. After device reset, all pereipheral clocks are disables which means that GPIO pins are not operable. Clock manipulation is done by means of writing to \texttt{RCC\_AHBxENR}\footnote{Replace x by a number of the bus peripheral is connected to. From the block diagram it is clear that \texttt{RCC\_AHB1ENR} controls clock for port D.} register. Section \textbf{Reset and clock control (RCC) - RCC registers - RCC AHB1} from the reference manual shows the register, its reset state, memory location and bits allocation. Setting \texttt{RCC\_AHB1ENR[3]}\footnote{Notice that counting starts with 0, therefore it is 4th least significant bit.} bit, writing logical 1, enables clock for port D of the microcontroller, therefore it is operating from that moment. 

\begin{lstlisting}[language=C, caption=Enabling port clock by writing to RCC register]
RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;            // Enable port D
\end{lstlisting}

\texttt{RCC\_AHB1ENR\_GPIODEN} is expanded to 0x00000008 and logical \textbf{OR} is performed with the current value of the \texttt{AHB1ENR} register. It is equivalent to \texttt{|= (1 << 3)} or \texttt{|= 8}

Note: C and C++ does not define identifiers or keywords presented in the line above. This are macro definitions and structures introduced by CMSIS, which will be observed in the next section.

When port D is enabled we may set pins 12 and 13 to be digital outputs with pull up/down or without\footnote{This of course could have been done prior to enabling the port.}.

\begin{lstlisting}[language=C, caption=Port D pins 12 and 13 output configuration]
GPIOD->MODER |= GPIO_MODER_MODE12_0 | GPIO_MODER_MODE13_0;
\end{lstlisting}

\texttt{GPIO\_MODER\_MODE12\_0} and \texttt{GPIO\_MODER\_MODE13\_0} evaluate to 0x01000000 and 0x04000000 respectively, setting 24th and 26th bits of the \texttt{GPIOD\_MODER} register\footnote{Again calculation starts at 0}. The register and the values to write are known from the \textbf{General-purpose I/Os} section of \textbf{RM0383}. 

State of GPIO port and each pin of it in particular are affected by six 32-bit registers, whereas each port has 10 associated registers, including six mentioned before. There is no need to remember bit allocation of each register, but understanding of their purpose and how to find information about it is a must in order to be able to operate with I/O of the MCU.

Default configuration sets internal clock to 16Mhz and switching a LED without a delay will produce flicker for human eye visible as a continuous light and may stay unnoticed even to high speed cameras. To make result of the configuration and code work visible to human a delay should be introduced. Very simple delay routine is presented below.

\begin{lstlisting}[language=C,caption=Delay routine]
void Delay(uint32_t pValue)
{
	for(; pValue > 0; pValue--);
}
\end{lstlisting}

Setting I/O pin to high or low state is achieved by writing to \texttt{GPIOx\_ODR} register. However, it is favorable to use \texttt{GPIOx\_BSRR} register as it provides a way to perform atomic operations, that cannot be interrupted.

Following code enables and disables LEDs after a small delay. Please refer to \textbf{Reference manual} and \textbf{CMSIS include files} to understand what values are being written to \texttt{GPIOD\_BSRR} register.

\begin{lstlisting}[language=C,caption=Enable/disable LED]
GPIOD->BSRR = GPIO_BSRR_BS13;		// Pin 13 of port D set High
Delay(1000000);						// Delay
GPIOD->BSRR = GPIO_BSRR_BR13;		// Pin 13 of port D set Low
Delay(1000000);
\end{lstlisting}

Full listings of this and all other tutorials and sources of the book you are currently reading are available from BitBucket repository. \texttt{https://bitbucket.org/synaps/stm32f4-project}
