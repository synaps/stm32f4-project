#include <stdio.h>

#include "stm32f4xx.h"                  // Device header

void InitGPIOPins()
{
	/* By default for ST microcontrollers
	* clock is not feed to peripherals. Thus,
	* we need to turn it on before using */
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;		// Enable port D
	/* Coufiguring pins 12 and 13 of port D
	* as digital outputs without pull up/down */
	GPIOD->MODER |= GPIO_MODER_MODE12_0 | GPIO_MODER_MODE13_0;
}

// Simple delay. Nothing to look at really 
void Delay(uint32_t pValue)
{
	for(; pValue > 0; pValue--);
}

int main()
{
	InitGPIOPins();		// Enable port D and configure pins
	
	GPIOD->BSRR = GPIO_BSRR_BS12;		// Switch on LED2
	while(1)
	{		// make it blink!
		GPIOD->BSRR = GPIO_BSRR_BS13;		// Switch on LED3
		Delay(1000000);		// Delay
		GPIOD->BSRR = GPIO_BSRR_BR13;
		Delay(1000000);
	}
	return 0;
}
