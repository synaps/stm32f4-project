#include <stdio.h>

#include "stm32f4xx.h"                  // Device header

void InitAll()
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;		// Enable port D
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
	GPIOD->MODER |= GPIO_MODER_MODE12_0;		// Output PP		// Green LED
	GPIOD->MODER |= GPIO_MODER_MODE13_0;		// Orange LED
	GPIOD->MODER |= GPIO_MODER_MODE15_1;		// ?? LED
	//GPIOD->AFR[0] |= 0x02000000;
	GPIOD->AFR[1] |= 0x20000000;		// Alternate function for PD15

	// enabele TIM4
	TIM4->PSC = 0xffff;
	TIM4->CR1 |= TIM_CR1_CEN;
	
	uint32_t HSEStatus = 0;
	GPIOD->BSRR = GPIO_BSRR_BS13;		// set Orange user LED
	RCC->CR |= RCC_CR_HSEON;		// enable HSE
	do
	{
		HSEStatus = RCC->CR & RCC_CR_HSERDY;
	} while(HSEStatus == 0);		// white until HSE is ready
	
	GPIOD->BSRR = GPIO_BSRR_BR13;		// clear Orange user LED
}

void Delay(uint32_t pValue)
{
	for(; pValue > 0; pValue--);
}

int main()
{
	InitAll();
	
	GPIOD->BSRR = GPIO_BSRR_BS12;
	while(1)
	{
		GPIOD->BSRR = GPIO_BSRR_BS13;
		Delay(1000000);
		GPIOD->BSRR = GPIO_BSRR_BR13;
		Delay(1000000);
	}
	return 0;
}

