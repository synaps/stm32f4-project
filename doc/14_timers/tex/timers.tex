\section{Timers}
STM32F4 has multiple timers on board. In datasheet you may encounter \textbf{advanced-control} and \textbf{general-purpose} timers.

The general-purpose timer in ARM microcontroller consists of a 16 or 32 bit auto-reload counter driven by a programmable prescaler. Timers 2 to 5 on STM32F411 may be used for a variety of purposes, including generation of output signals and measuring of input signal pulse lengths. The timers are completely independent and they do not share any resources.

Figure \ref{figure:timer-block} depicts block diagram of a general-purpose timer. Main block of the timer is denoted with CNT abbreviation, stands for counter, in the diagram and has a related auto-reload register. Timer 3 (TIM3), that will be used later in the tutorial, is a 16-bit timer, therefore CNT and auto-reload register (TIMx\_ARR) are 16 bit long. The counter is clocked by the prescaler output CK\_PSC and can device the counter clock frequency by any factor between 1 and 65536. The counter clock can be provided by several sources both internal and external. General-purpose counters can count both up and down.
\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{gen-timer-block.png}
	\caption{General-purpose timer block diagram}
	\label{figure:timer-block}
\end{figure}

Counter has three different counter modes: upcounting, downcounting and center-aligned (up/down counting) mode. In upcounting mode the counter counts from 0 to the value of auto-reload register (TIMx\_ARR), then restarts from 0 and generates an overflow event. In downcounting mode the counter counts from the auto-reload value (content of the TIMx\_ARR register) down to 0, then restarts from the auto-reload value and generates a counter underflow event. In center-aligned mode the counter counts from 0 to the auto-reload value - 1, generates an overflow event, then counts from the auto-reload value down to 1 and generates an underflow event. After that cycle restarts from 0.

Update event is generated every time timer reaches the overflow or underflow. Many registers including prescaler, shadow registers and prescaler buffers.

\subsection{Exmaple}
\subsubsection{Task}
This example, provided by STMicroelectronics, shows how to configure the TIM (timer) peripheral to generate a time base of one second with the corresponding IRQ (Interrupt request). 

\subsubsection{Solution}
In this example TIM3 input clock (TIM3CLK) is set to 2 times the APB1 clock (PCLK1). 
\[\text{TIM3CLK} = 2 * \text{PCLK1}\]
\[\text{PCLK1} = \frac{\text{HCLK}}{2}\]
\[\therefore \text{TIM3CLK} = \text{HCLK} = \text{SystemCoreClock}\]

To calculate a prescaler for a timer clock following formula is used.
\[\text{Prescaler} = \frac{\text{TIM3CLK}}{\text{TIM3 counter clock}} - 1\]

When the counter value reaches the auto-reload register value, the TIM update interrupt is generated and, in the handler routine pin 12 of port D is toggled with the frequency of 0.5Hz.

In this tutorial HAL library is used. Therefore, after acquaintance with \textbf{General-purpose} section in microcontroller reference manual turn to \textbf{HAL TIM Generic driver} section of the HAL user manual. In this section data structures, functions required for configuration and manipulation of the module are listed and explained. Proper sequences of actions, for achieving different functionality, are also given in that section. Basic time based use of the timer would require following functions to be executed:
\begin{enumerate}
	\item Initialize the timer low level resources. For time base features it is \texttt{HAL\_TIM\_Base\_MspInit()}.
	\item Enable clock and configure required pins. \texttt{\_\_TIMx\_CLK\_ENABLE()} for enabling clock and \texttt{\_\_GPIOx\_CLK\_ENABLE()} and configuring alternate function with \texttt{HAL\_GPIO\_Init()}.
	\item Clock configuration should be done before any start function.
	\item Configure timer in the desired functioning mode. \texttt{HAL\_TIM\_Base\_Init()} to use timer to generate a sime time base.
	\item Activate timer. For time base function \texttt{HAL\_TIM\_Base\_Start()}, \texttt{HAL\_TIM\_Base\_Start\_DMA()} or \texttt{HAL\_TIM\_Base\_Start\_IT()}.
	\item DMA Burst should be handled.
\end{enumerate}

In main function HAL is being initialized first. Then prescaler value is calculated to have timer clock counter equal to 10kHz by dividing SystemCoreClock, that after calling \texttt{SystemClock\_Config()} was set to \(100*10^6\)Hz, by 10000 (10kHz) and subtracting 1 from it.
\begin{lstlisting}[language=C, caption=Calculation of prescaler value]
/* Compute the prescaler value to have TIM3 counter clock equal to 10 KHz */
uwPrescalerValue = (uint32_t) ((SystemCoreClock / 10000) - 1);
\end{lstlisting}

After that timer module is being configured. HAL configuration is self-explanatory. With the frequency of 5kHz timer will count up to 99999 and then restart from 0. APB1 that TIM3 is connected to is two times slower that SYSCLK, therefore prescaler stored in \texttt{uwPrescalerValue} will divide 50Mhz and resultant TIM3 clock frequency after scaling will be 5kHz. Thus, to reach 10000 (period of the timer) two seconds will be required, so timer overflows once every two seconds.
\begin{lstlisting}[language=C,caption=TIM3 configuration and starting]
TIM_HandleTypeDef    TimHandle;		// Declared outside of main function
//---
TimHandle.Instance = TIMx;	// Set TIMx instance; TIMx = TIM3
TimHandle.Init.Period = 10000 - 1;	// TIMx_ARR value
TimHandle.Init.Prescaler = uwPrescalerValue;
TimHandle.Init.ClockDivision = 0;
TimHandle.Init.CounterMode = TIM_COUNTERMODE_UP;	// upcounting mode
if(HAL_TIM_Base_Init(&TimHandle) != HAL_OK)
  Error_Handler();		// Initialization error

/*-2- Start the TIM Base generation in interrupt mode */
if(HAL_TIM_Base_Start_IT(&TimHandle) != HAL_OK)
  Error_Handler();		// Starting error
\end{lstlisting}

\texttt{HAL\_TIM\_Base\_Start\_IT} function call means that timer generation is in interrupt mode, i.e. generation will be generated every time counter overflows. This will happen at frequency of 0.5Hz (once every two seconds). In addition to interrupt HAL callback function will be called. While timer interrupt handler in \texttt{stm32f4xx\_it.c} does not execute any user code, until you write your own, callback function \texttt{HAL\_TIM\_PeriodElapsedCallback()} toggles the green LED.
