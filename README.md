# Development STM32F4 material

This work aims to create easy to follow and understand tutorial on how to start working with the STM32F4 series microcontroller with the help of STM32F4 discovery board.

Tutorials try to teach how to use documentation provided by the manufacturer.

# Contents of the repository

Git repository currently contains only TEX and project files. Graphics is not included and the latest version of all documents are freely available from the web.
